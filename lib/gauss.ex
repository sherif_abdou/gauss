defmodule Gauss do
  defmodule Struct do
    defstruct [:data]
  end

  defp per_row({i, x}) do
    IO.inspect(i)
    IO.inspect(x)
    (x |> Enum.take_while(&(&1 == 0))) == i
  end

  def is_row_eschelon(matrix) do
    IO.inspect(matrix)
    Enum.zip(Stream.iterate(0, &(&1 + 1)), matrix) |> Enum.all?(&per_row/1)
  end
end
